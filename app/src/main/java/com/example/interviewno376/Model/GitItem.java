package com.example.interviewno376.Model;

public class GitItem {
    String id,name,html_url,description;

    public GitItem(String id, String name, String html_url, String description) {
        this.id = id;
        this.name = name;
        this.html_url = html_url;
        this.description = description;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getHtml_url() {
        return html_url;
    }

    public void setHtml_url(String html_url) {
        this.html_url = html_url;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }
}
